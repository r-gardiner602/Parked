Rails.application.routes.draw do
  post 'authenticate' => 'auth#authenticate'

  match '*all', to: 'application#preflight', via: [:options]

  get 'current_user', to: 'application#current_user'
  get 'request_token', to: 'tokens#request_token'
  get 'access_token', to: 'tokens#access_token'
  resources :blabs, only: [:index]
end