class UsersController < ApplicationController
  validates :password, length: { minimum: 8 }, on: :create
  validates :email, uniqueness: true, email_format: true
  validates_presence_of :uid, :email
end
